import React, { useContext } from 'react';
import { Redirect, Route } from 'react-router-dom';
import HomeLayout from '../modules/Layout/HomeLayout';
import { isLoggedIn } from '../utils/AuthUtils';
import { RouteContext } from './routes';

export default ({ loggedIn, component: Component, ...rest }) => {
    const [user, setUser] = useContext(RouteContext);
    setUser(loggedIn);

    let Comp = Component;

    const pathname = window.location.pathname;

    if(!Comp){
        return (
            <Route
                {...rest}
                render={props =>
                    isLoggedIn(loggedIn)? // is authenticated
                        <HomeLayout user={loggedIn}>
                            <div {...props}> </div>
                        </HomeLayout>
                        :
                        <Redirect to={{ pathname: '/login', search: `?continue=${encodeURI(window.location.href)}`, state: { from: props.location } }} />
                }
            />
        )
    }

    return (
        <Route
            {...rest}
            render={props =>
                isLoggedIn(loggedIn)? // is authenticated
                    <HomeLayout user={loggedIn}>
                        <Comp {...props} />
                    </HomeLayout>
                    :
                    <Redirect to={{ pathname: '/login', search: `?continue=${encodeURI(window.location.href)}`, state: { from: props.location } }} />
            }
        />
    )
}
