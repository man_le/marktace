import { isEmpty } from 'lodash';
import { CON_TYPE, DB, fireStore } from '../utils/DBUtils';
import type from './type';

const updateFollowing = async (dispatch, { followingUserIds, followingResult }) => {

    // Get following users
    let folowingUsers = [];
    if (!isEmpty(followingUserIds)) {

        // Seperate big array to multiple array with 10 items inside
        let maxLen = 100;
        if (followingUserIds.length < 100) maxLen = followingUserIds.length;
        let arrayMax10 = [];
        let step = 0;
        for (let i = 0; i <= maxLen / 10; i++) {
            arrayMax10.push(followingUserIds.slice(step, step + 10));
            step += 10;
        }

        for (let i = 0; i < arrayMax10.length; i++) {
            if (!isEmpty(arrayMax10[i]) && arrayMax10[i].length <= 10) {
                const result = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.IN, arrayMax10[i]]);
                if (!isEmpty(result)) folowingUsers.push(...result);
            }
        }
        // End seperating

        dispatch({ type: type.UPDATE_FOLLOWING, following: folowingUsers, followingResult: followingResult || [] });
    } else {
        dispatch({ type: type.UPDATE_FOLLOWING, following: [], followingResult: [] });
    }
    return folowingUsers;
}

const updateFollower = async (dispatch, { followingUserIds, followerUserIds, followerResult }) => {

    // Get following users
    let followerUsers = [];
    if (!isEmpty(followerUserIds)) {

        // Seperate big array to multiple array with 10 items inside
        let maxLen = 100;
        if (followerUserIds.length < 100) maxLen = followerUserIds.length;
        let arrayMax10 = [];
        let step = 0;
        for (let i = 0; i <= maxLen / 10; i++) {
            arrayMax10.push(followerUserIds.slice(step, step + 10));
            step += 10;
        }
        for (let i = 0; i < arrayMax10.length; i++) {
            if (!isEmpty(arrayMax10[i]) && arrayMax10[i].length <= 10) {
                const result = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.IN, arrayMax10[i]]);
                if (!isEmpty(result)) followerUsers.push(...result);
            }
        }
        // End seperating

        // Adapt to show button Follow/Un-Follow in Follower Modal
        if (!isEmpty(followerUsers)) {
            followerUsers.some((e, i) => {
                if (followingUserIds.indexOf(e._id) !== -1) {
                    followerUsers[i].following = true;
                    return true;
                }
            })
        }
        dispatch({ type: type.UPDATE_FOLLOWER, follower: followerUsers, followerResult: followerResult || [] });
    } else {
        dispatch({ type: type.UPDATE_FOLLOWER, follower: [], followerResult: [] });
    }
    return followerUsers;
}

const updateFollowingOffline = (dispatch, { folowingUsers, useridUnfollow }) => {
    dispatch({ type: type.UPDATE_FOLLOWING_OFFLINE, following: folowingUsers, useridUnfollow });
}


export {
    updateFollowing,
    updateFollowingOffline,

    updateFollower,
}