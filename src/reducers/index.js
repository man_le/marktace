import type from './type';
import { updateFollowing, updateFollowingOffline, updateFollower } from './followEpic';

export default (state = {}, action) => {
  switch (action.type) {
    case type.SHOW_WELCOME_PAGE: return { ...state, hideWelcomePage: action.hideWelcomePage }
    case type.SHOW_HIDE_MESSAGE: return { ...state, isHideMessage: action.isHideMessage }
    case type.ADD_NEW_MESSAGE: return { ...state, status_id: action.status_id, addNewMessage: true };
    case type.ADD_NEW_STATUS: return { ...state, addNewMessage: false };
    case type.HAS_NEW_MESSAGE: return { ...state, hasNewMessage: true, data: action.data };
    case type.NO_NEW_MESSAGE: return { ...state, hasNewMessage: false };
    case type.SHOW_LIKE_MODAL: return { ...state, likes: action.likes }

    // Following
    case type.UPDATE_FOLLOWING: return { ...state, following: action.following }
    case type.UPDATE_FOLLOWING_OFFLINE:
      const index = state.follower.findIndex(e => e._id === action.useridUnfollow);
      const newFollower = state.follower;
      if (index !== -1) {
        newFollower[index].following = false;
      }
      return { ...state, following: action.following, follower: newFollower }
    case type.UPDATE_FOLLOWER: return { ...state, follower: action.follower }

    // Like status
    case type.BUILD_NEW_LIKE: return { ...state, newLikes_userIds: action.newLikes_userIds, newLikes_users: action.newLikes_users }


  }

}

export {
  updateFollowing,
  updateFollowingOffline,
  updateFollower,
}