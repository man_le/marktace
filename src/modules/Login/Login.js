import { isEmpty } from 'lodash';
import React, { useRef, useState } from 'react';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { Link } from 'react-router-dom';
import { facebookLogin, googleLogin, login } from '../../utils/AuthUtils';
import { validate } from '../../utils/ValidateUtils';
import R from '../../locale/R';
import { DB, CON_TYPE, fireStore } from '../../utils/DBUtils';
import Footer from './Footer';
import dAndroid from './Static/img/downloadandroid.png';
import dIos from './Static/img/downloadios.png';
import './stylesheet.scss';
import { buildKeyWords } from '../../utils/StringUtils';

const signInHandler = (email, password, localState, setLocalState) => {
    if (isEmpty(email) || isEmpty(password)) return;
    if (localState.isLogin === false) {
        setLocalState({ isLogin: true });
        login(email, password, async code => {
            setLocalState({ isLogin: false });
            if (isEmpty(code)) {
                NotificationManager.success(R.access.login_success, '', 3000);
                setTimeout(() => { window.location.reload(true) }, 1000)
            } else {
                if (code === 'auth/email-already-in-use') NotificationManager.error(R.access.email_exist, '', 3000);
                else if (code === 'auth/invalid-email') NotificationManager.error(R.common.email_not_valid, '', 3000);
                else if (code === 'auth/weak-password') NotificationManager.error(R.access.pass_at_least_6, '', 3000);
                else if (code === 'auth/wrong-password') NotificationManager.error(R.access.wrong_pass_email, '', 3000);
                else NotificationManager.error(R.access.common_error, '', 3000);
            }
        });
    }
}

const fbLoginHandler = () => {
    facebookLogin(async (code, userInfo) => {
        if (isEmpty(code)) {
            const user = userInfo;
            const data = {
                name: user.displayName,
                avatar: user.photoURL,
                userUid: user.uid,
                link: '',
                notify_like_comment: true,
                notify_like_status: true,
                notify_share_status: true,
                notify_post_new: true,
                notify_comment: true,
                noti_following: true,
                email: user.email,
                social_id: !isEmpty(user.providerData) ? user.providerData[0].uid : null,
                phoneNumber: user.phoneNumber,
                keywords: buildKeyWords(user.displayName)
            }
            // await fireStore.update(DB.USER.name(), [DB.USER.USERUID, CON_TYPE.EQ, user.uid], data, true);

            NotificationManager.success(R.access.login_success, '', 3000);
            setTimeout(() => { window.location.reload(true) }, 1000)
        } else {
            if (code === 'auth/account-exists-with-different-credential') NotificationManager.error(R.access.email_linked_fb, '', 3000);
            else NotificationManager.error(R.access.common_error, '', 3000);
        }
    });
}

const ggLoginHandler = () => {
    googleLogin(async (code, userInfo) => {
        if (isEmpty(code)) {

            const user = userInfo;
            const data = {
                name: user.displayName,
                avatar: user.photoURL,
                userUid: user.uid,
                link: '',

                email: user.email,
                phoneNumber: user.phoneNumber,
                social_id: !isEmpty(user.providerData) ? user.providerData[0].uid : null,
                notify_like_comment: true,
                notify_like_status: true,
                notify_share_status: true,
                notify_post_new: true,
                notify_comment: true,
                noti_following: true,
                keywords: buildKeyWords(user.displayName)
            }
            // await fireStore.update(DB.USER.name(), [DB.USER.USERUID, CON_TYPE.EQ, user.uid], data, true);

            NotificationManager.success(R.access.login_success, '', 3000);
            setTimeout(() => { window.location.reload(true) }, 1000);
        } else {
            if (code === 'auth/account-exists-with-different-credential') NotificationManager.error(R.access.email_linked_fb, '', 3000);
            else NotificationManager.error(R.access.common_error, '', 3000);
        }
    });
}


const Login = () => {

    const [localState, setLocalState] = useState({ isLogin: false, disabledBtn: true });
    const emailRef = useRef('');
    const passRef = useRef('');

    const onChangeHandler = ()=>{
        if(!isEmpty(emailRef.current.value) && !isEmpty(passRef.current.value)){
            setLocalState({...localState, disabledBtn:false});
        }else{
            setLocalState({...localState, disabledBtn:true});
        }
    }

    return (
        <div className='container access-group noselect'>
            <div className='row'>
                <div className='col-md-3'>
                    <div className='zlogo'>
                        <img src='https://zapii.me/assets/data/logo.png' />
                    </div>
                    <div className='x-login'>
                        <i className="fa fa-facebook-square"></i>
                        <button onClick={fbLoginHandler} type='button' className='tfb zpointer'>{R.access.fbLogin}</button>
                    </div>

                    <div className='x-login ls'>
                        <i className="fa fa-google-plus"></i>
                        <button onClick={ggLoginHandler} type='button' className='tgg zpointer'>{R.access.ggLogin}</button>
                    </div>

                    <div className='spl'>
                        <div className='ln'>
                            <div className='l1'></div>
                        </div>
                        <div className='tx'>
                            {R.access.or}
                        </div>
                        <div className='ln'>
                            <div className='l1'></div>
                        </div>
                    </div>
                    <div className='zi'>
                        <input maxLength={50} ref={emailRef} onKeyUp={onChangeHandler} onChange={onChangeHandler} type="email" className='zt' placeholder='Email'/>
                    </div>
                    <div className='zi'>
                        <input maxLength={10} ref={passRef} onKeyUp={onChangeHandler} onChange={onChangeHandler} type="password" className='zt' placeholder='Password'/>
                    </div>
                    <div className='zi2'>
                        <button type='button' disabled={localState.isLogin || localState.disabledBtn} onClick={() => signInHandler(emailRef.current.value, passRef.current.value, localState, setLocalState)}  className={`zabtn zscale-effect2 ${localState.disabledBtn ? 'zdisabled':''}`}>{R.access.login}</button>
                    </div>
                    <div className='zbt'>
                        <Link to='/forgot' className='lk'>
                            {R.access.forgot_pass}
                        </Link>
                        <Link to='/reg' className='lk'>
                            {R.access.sign_up}
                        </Link>
                    </div>
                </div>

                <div className='dng col-md-3'>
                    <div className='dngt'>
                        {R.access.downloadapp}
                    </div>
                    <div className='appg'>
                        <img className='android zdisabled' src={dAndroid} />
                        <img className='iod zdisabled' src={dIos} />
                    </div>
                </div>

                <Footer />
                <NotificationContainer />

            </div>

        </div>

    );
}

export default Login;
