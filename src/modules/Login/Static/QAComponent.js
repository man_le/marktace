import React from 'react';
import { Link } from 'react-router-dom';
import LogoImg from './img/zapiilogo.png';
import ShirtImg from './img/icon/shirt.jpg';
import BookmarkImg from './img/icon/bookmark.jpg';
import MixImg from './img/icon/mix.jpg';
import CurrImg from './img/icon/curr.jpg';
import BagImg from './img/icon/bag.jpg';
import './stylesheet_qa.scss';

const Comp = () => {
    return (
        <div className='container qa-group'>
            <div className='row'>
                <div className='col-md-6'>
                    <div className='ztop'>
                        <img className='top-logo' src={LogoImg} />
                        {
                            window.location.pathname.indexOf('_qa') !== -1
                                ?
                                <Link to='/' className='top-link'>
                                    Trang Chủ
                                </Link>
                                :
                                <Link to='/login' className='top-link'>
                                    Đăng nhập
                                </Link>
                        }
                    </div>
                    <div className='zdetail'>
                        <div className='hd'>
                            Câu hỏi thường gặp
                        </div>

                        <div className="accordion" id="accqa">

                            <div className="card">
                                <div className="card-header" id="hqa1">
                                    <h2 className="mb-0">
                                        <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#qa1" aria-expanded="true" aria-controls="qa1">
                                            1. Zapii là gì ?
                                        </button>
                                    </h2>
                                </div>

                                <div id="qa1" className="collapse show" aria-labelledby="hqa1" data-parent="#accqa">
                                    <div className="card-body">
                                        Zappi là dịch vụ giúp bạn tự tạo các bản phối đồ dựa trên các sản phẩm thời trang có sẳn, sau đó chia sẻ hoặc mua sắm trực tuyến ngay trên ứng dụng với giá ưu đãi riêng tùy vào số điểm bạn tích lũy.
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header" id="hqa2">
                                    <h2 className="mb-0">
                                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#qa2" aria-expanded="true" aria-controls="qa2">
                                            2. Điểm tích lũy là gì?
                                        </button>
                                    </h2>
                                </div>

                                <div id="qa2" className="collapse" aria-labelledby="hqa2" data-parent="#accqa">
                                    <div className="card-body">
                                        Là số điểm bạn dùng để mua sắm ưu đãi trên ứng dụng, mỗi 1 điểm sẽ giảm tương ứng 1,000đ khi mua sắm. Zapii sẽ tặng bạn 1 điểm tích lũy khi mỗi bài đăng của bạn có trên 30 lượt yêu thích. Điểm tích lũy tối đa là 100.
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header" id="hqa3">
                                    <h2 className="mb-0">
                                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#qa3" aria-expanded="true" aria-controls="qa3">
                                            3. Các bước để phối đồ và thêm set đồ đã phối vào giỏ hàng?
                                        </button>
                                    </h2>
                                </div>

                                <div id="qa3" className="collapse" aria-labelledby="hqa3" data-parent="#accqa">
                                    <div className="card-body">
                                        <div>
                                            <ul>
                                                <li>
                                                    Trên thanh trình đơn, nhấp chọn <img src={ShirtImg} style={{ width: '1.5rem' }} /> để vào mục Khám Phá
                                                </li>
                                                <li>
                                                    Danh sách sản phẩm hiện ra, tiến hành thêm sản phẩm vào "Nguyên Liệu Phối Đồ" bằng cách nhấp chọn biểu tượng <img src={CurrImg} style={{ width: '1.5rem' }} /> hoặc nhấp chọn  <img src={BookmarkImg} style={{ width: '1.25rem' }} /> để thêm sản phẩm vào "Danh Sách Yêu Thích".
                                                </li>
                                                <li>
                                                    Trên thanh trình đơn, nhấp chọn <img src={MixImg} style={{ width: '1.5rem' }} /> để vào mục Phối Đồ
                                                </li>
                                                <li>
                                                    Ở giao diện Phối Đồ, bạn có thể lưu lại set đồ đã phối, hoặc thêm set đồ vào giỏ hàng bằng nhấp vào biểu tượng "..." chọn "Thêm Set Đồ Vào Giỏ".
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header" id="hqa4">
                                    <h2 className="mb-0">
                                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#qa4" aria-expanded="true" aria-controls="qa4">
                                            4. Làm sao để chia sẻ đồ đã phối lên tường?
                                        </button>
                                    </h2>
                                </div>

                                <div id="qa4" className="collapse" aria-labelledby="hqa4" data-parent="#accqa">
                                    <div className="card-body">
                                        Bạn vào Trang Cá Nhân, chọn mục "Set Đồ", nhấp chọn biểu tượng "..." ở set đồ bạn muốn, sau đó chọn Chia Sẻ.
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header" id="hqa5">
                                    <h2 className="mb-0">
                                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#qa5" aria-expanded="true" aria-controls="qa5">
                                            5. Tôi muốn mua hàng thì phải làm sao?
                                        </button>
                                    </h2>
                                </div>

                                <div id="qa5" className="collapse" aria-labelledby="hqa5" data-parent="#accqa">
                                    <div className="card-body">
                                        <div>
                                            <ul>
                                                <li>
                                                    Trên thanh trình đơn, nhấp chọn <img src={ShirtImg} style={{ width: '1.5rem' }} /> để vào mục Khám Phá
                                                </li>
                                                <li>
                                                    Danh sách sản phẩm hiện ra, nhấp chọn <img src={BagImg} style={{ width: '1.5rem' }} /> ở sản phẩm bạn muốn mua
                                                </li>
                                                <li>
                                                    Ở trang chi tiết sản phẩm, bạn chọn size của sản phẩm muốn mua, sau đó nhấp chọn "Thêm Vào Giỏ Hàng"
                                                </li>
                                                <li>
                                                    Trên thanh trình đơn, nhấp chọn <img src={BagImg} style={{ width: '1.5rem' }} /> để vào mục Giỏ Hàng, tại đây bạn có thể thực hiện việc kiểm tra, thanh toán và đặt hàng.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header" id="hqa7">
                                    <h2 className="mb-0">
                                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#qa7" aria-expanded="true" aria-controls="qa7">
                                            6. Tôi nhận được hàng nhưng sau đó muốn trả lại thì có được không?
                                        </button>
                                    </h2>
                                </div>

                                <div id="qa7" className="collapse" aria-labelledby="hqa7" data-parent="#accqa">
                                    <div className="card-body">
                                        <div>Zapii chỉ chấp nhận đổi trả các sản phẩm thỏa điều kiện sau:</div>
                                        <ul style={{ marginTop: '0.25rem', marginLeft: '1rem' }}>
                                            <li>
                                                Thời gian đổi hàng trong vòng 30 ngày khi xuất hóa đơn
                                            </li>
                                            <li>
                                                Không chấp nhận việc trả hàng để lấy lại tiền mặt trong bất cứ trường hợp nào
                                            </li>
                                            <li>
                                                Việc đổi trả chỉ được thực hiện khi sản phẩm bị lỗi hoặc những sự cố phát sinh do lỗi từ phía Zapii
                                            </li>
                                            <li>
                                                Chỉ chấp nhận việc đổi hàng khi sản phẩm chưa qua sử dụng, còn nhãn mác và hóa đơn mua hàng.
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div className="card">
                                <div className="card-header" id="hqa8">
                                    <h2 className="mb-0">
                                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#qa8" aria-expanded="true" aria-controls="qa8">
                                            7. Tôi muốn báo cáo với ban quản trị về một bài đăng phản cảm thì phải làm sao?
                                        </button>
                                    </h2>
                                </div>

                                <div id="qa8" className="collapse" aria-labelledby="hqa8" data-parent="#accqa">
                                    <div className="card-body">
                                        Nhấp vào dấu "..." ở bài đăng bạn muốn báo cáo, chọn "Báo Cáo".
                                    </div>
                                </div>
                            </div>

                            <div className="card-header" id="hqa9">
                                <h2 className="mb-0">
                                    <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#qa9" aria-expanded="true" aria-controls="qa9">
                                        8. Tôi muốn hợp tác với Zapii thì liên hệ như thế nào?
                                        </button>
                                </h2>
                            </div>

                            <div id="qa9" className="collapse" aria-labelledby="hqa9" data-parent="#accqa">
                                <div className="card-body">
                                    Bạn có thể liên hệ chúng tôi qua
                                    <div>Email: <a href="mailto:feedback@zapii.me">feedback@zapii.me</a></div>
                                    <div>Điện thoại: <strong>0907.371.354</strong> hoặc <strong>0943.127.275</strong></div>
                                </div>
                            </div>

                        </div>

                        <div className='endqa'>
                            Bạn vẫn còn câu hỏi?, hãy gởi email về địa chỉ <a href="mailto:feedback@zapii.me">feedback@zapii.me</a>.
                        </div>

                    </div>
                </div>
            </div>

            <div className='row'>
                <div className='col-md-6 zcontact'>
                    <div className='it'>
                        Cập nhật sau cùng ngày 14/05/2020
                    </div>
                    <div className='it'>
                        <a href="feedback@zapii.me">feedback@zapii.me</a>
                    </div>
                    <div className='it'>
                        ©Zapii.me {new Date().getFullYear()}
                    </div>

                </div>
            </div>
        </div>
    )
}

export default Comp;