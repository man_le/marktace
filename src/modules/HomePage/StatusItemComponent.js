import { isEmpty } from 'lodash';
import React, { useContext, useEffect, useState } from 'react';
import type from '../../reducers/type';
import { RouteContext } from '../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import { GlobalStateContext } from '../Layout/HomeLayout';
import R from './../../locale/R';
import MessageItemComponent from './MessageItemComponent';
import './stylesheet.scss';

const Comp = ({ e, index }) => {

    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [localState, setLocalState] = useState({ items: [], init: false, loading: false, lastVisible: null, more: true });
    const [user, setUser] = useContext(RouteContext);
    const { isHideMessage } = globalState;

    const fetchData = async (hasNew) => {
        let { items, lastVisible } = localState;

        if (hasNew) {
            if (globalState.data && globalState.data.status_id === e._id) items.splice(1, 0, globalState.data);
        }

        const result = await fireStore.findLimitMultiConditions(
            DB.STATUS.name(),
            12,
            lastVisible,
            null,
            DB.STATUS.CREATED_DATE,
            'desc',
            [DB.STATUS.IN_STATUS_ID, CON_TYPE.EQ, e._id],
            [DB.STATUS.IS_OWNER, CON_TYPE.EQ, false],
        );
        let more = true;
        if (isEmpty(result) || isEmpty(result.data) || result.data.length < 12) {
            more = false;
        }
        const data = result.data;
        for (let i = 0; i < data.length; i++) {
            const findData = items.find(e => e._id === data[i]._id);
            let dataAdded = data[i];
            dataAdded.marginLeft = `${Math.floor(Math.random() * 21) + 1}rem`;
            dataAdded.marginRight = `${Math.floor(Math.random() * 11) + 1}rem`;
            if (isEmpty(findData)) items.push(dataAdded);
        }
        lastVisible = result.lastVisible;
        setLocalState({ ...localState, items, lastVisible, init: true, loading: false, more });
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {

            if (globalState.hasNewMessage) {
                fetchData(true);
                dispatch({ type: type.NO_NEW_MESSAGE })
            } else {
                fetchData();
            }

            loadCompleted = true;
        }

        return () => {
            return loadCompleted == true;
        }

    }, [globalState.hasNewMessage === true]);

    const addNewMessageHandler = () => {
        dispatch({ type: type.ADD_NEW_MESSAGE, status_id: e._id });
    }

    const { items, more, loading, init } = localState;

    if (isEmpty(items) && !init) {
        return null;
    }

    return (

        <div key={'status_' + e._id} className='bckg' style={{ background: `lightblue url(${e.backgroundImg}) no-repeat center`, height: `${window.innerHeight - 45}px` }}>
            <div className='trgroup'>
                <button onClick={addNewMessageHandler} data-toggle={"modal"} data-target={`#new_status_modal`} type='button noselect' className='nmsg'>
                    <i className='material-icons'>add</i>
                    <div>{R.new.add_msg}</div>
                </button>
            </div>
            <div className='author-group'>
                <MessageItemComponent isOwner={e.isOwner || false} e={e} hide={isHideMessage && !e.isOwner}/>
                {
                    items.map((msg, index) =>
                        <MessageItemComponent key={`msg_${msg._id}`} e={msg} isRight={index % 2 === 0} hide={isHideMessage}/>
                    )
                }
            </div>
            {
                items.length >= 12 && more && !isHideMessage &&
                <div className='more-message'>
                    <div onClick={fetchData} className='tx'>
                        {R.new.load_msg}
                    </div>
                </div>
            }
        </div>

    )
}

export default Comp;