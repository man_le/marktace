import React, { useContext, useEffect, useState } from 'react';
import { isEmpty } from 'lodash';
import { BounceLoader } from 'react-spinners';
import MessageItemComponent from './MessageItemComponent';
import InfiniteScroll from "react-infinite-scroll-component";
import { RouteContext } from '../../routes/routes';
import { GlobalStateContext } from '../Layout/HomeLayout';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import StatusItemComponent from './StatusItemComponent';
import type from '../../reducers/type';
import './stylesheet.scss';

const Comp = () => {

    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [localState, setLocalState] = useState({ items: [], init: false, loading: false, lastVisible: null, more: true });
    const [user, setUser] = useContext(RouteContext);
    const { isHideMessage } = globalState;

    const fetchData = async () => {
        let { items, lastVisible } = localState;

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {
            const userInfo = user.extData[0];
            const result = await fireStore.findLimitMultiConditions(
                DB.STATUS.name(),
                12,
                lastVisible,
                null,
                DB.STATUS.CREATED_DATE,
                'desc',
                [DB.STATUS.IS_OWNER, CON_TYPE.EQ, true],
            );
            let more = true;
            if (isEmpty(result) || isEmpty(result.data)) {
                more = false;
            } else {
                const data = result.data;
                for (let i = 0; i < data.length; i++) {
                    const findData = items.find(e => e._id === data[i]._id);
                    if (isEmpty(findData)) items.push(data[i]);
                }
                lastVisible = result.lastVisible;
            }

            setLocalState({ ...localState, items, lastVisible, init: true, loading: false });
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }

        return () => {
            return loadCompleted == true;
        }

    }, [user]);

    const { items, more, loading, init } = localState;

    if (isEmpty(items) && !init) {
        return (
            <div className={`zloading-item`}><BounceLoader
                sizeUnit={"px"}
                size={50}
                loading={true}
                color='#777'
            /></div>
        )
    }

    return (
        <div className='zmain home-group'>
            <InfiniteScroll
                dataLength={items.length}
                next={fetchData}
                hasMore={more}
            >
                {
                    items.map((e,i) =>
                        <StatusItemComponent key={'status_'+e._id} e={e} index={i}/>
                    )
                }


            </InfiniteScroll>
        </div>

    )
}

export default Comp;