import React, { useEffect, useState, useContext } from 'react';
import Avatar from 'react-avatar';
import { GlobalStateContext } from '../Layout/HomeLayout';
import * as firebase from "firebase/app";
import moment from 'moment';
import type from '../../reducers/type';
import { formatMoment } from '../../utils/StringUtils';
import R from './../../locale/R';
import { RouteContext } from '../../routes/routes';
import { CON_TYPE, DB, fireStore } from '../../utils/DBUtils';
import { isEmpty } from 'lodash';

const Comp = ({ isRight, isOwner, hide, e }) => {
    let cls = '';
    let style = {};
    if (!isOwner) {
        if (isRight) {
            cls = 'right';
            style = { marginRight: e.marginRight };
        } else {
            cls = '';
            style = { marginLeft: e.marginLeft }
        }
    }

    if (hide) {
        style.display = 'none';
    }

    const [localState, setLocalState] = useState({ userInfo: null, init: false, isLiked: false });
    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [user, setUser] = useContext(RouteContext);

    const loggedUser = user.extData[0];

    const fetchData = async () => {
        let {isLiked} = localState;
        if(e.likes && e.likes.indexOf(loggedUser._id)!==-1){
            isLiked = true;
        }else{
            isLiked = false;
        }

            let result = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, e.user_id]);
            if (!isEmpty(result)) {
                result = result[0];
                setLocalState({ ...localState, userInfo: result, init: true, isLiked });
            } else {
                setLocalState({ ...localState, init: true, isLiked });
            }
    
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }

        return () => {
            return loadCompleted == true;
        }

    }, []);

    const likeHandler = () => {

        // Unlike
        if (localState.isLiked === true) {
            const data = {
                likes: firebase.firestore.FieldValue.arrayRemove(loggedUser._id),
            }
            fireStore.updateMultiConditions(DB.STATUS.name(), data, false, [DB.STATUS.ID, CON_TYPE.EQ, e._id]);
            setLocalState({...localState, isLiked:false});
        } else {
            // Like
            const data = {
                likes: firebase.firestore.FieldValue.arrayUnion(loggedUser._id),
            }
            fireStore.updateMultiConditions(DB.STATUS.name(), data, false, [DB.STATUS.ID, CON_TYPE.EQ, e._id]);
            setLocalState({...localState, isLiked:true});
        }
    }

    const showLikeHandler = ()=>{
        console.log(e.likes)
        dispatch({type:type.SHOW_LIKE_MODAL, likes: e.likes || []});
    }

    const { userInfo, init } = localState;
    if (isEmpty(userInfo)) return null;

    const userOwner = userInfo;

    return (
        <div className={`info noselect ${cls}`} style={style}>
            <Avatar className='zavatar' src={userOwner.avatar} name={userOwner.name} size='2rem' round={true} />
            <div className='r'>
                <div className='name'>
                    {userOwner.name}
                    {isOwner && <i className='material-icons'>check_circle</i>}
                    {
                        e.wasHere &&
                        <span className='wh'>- {R.new.was_here}</span>
                    }
                </div>
                <div className='at'>
                    {formatMoment(moment, e.updatedDate)}
                    {
                        !isEmpty(e.location)
                        &&
                        <span className='locg'>
                            <i className='material-icons'>place</i>
                            <span>{e.location}</span>
                        </span>
                    }
                </div>
                <div className='message'>
                    {e.message}
                </div>
                <div className='actg'>
                    <div className='like-group'>
                        <i onClick={likeHandler} className={`material-icons ${localState.isLiked?'selected':''}`}>favorite</i>
                        <div onClick={showLikeHandler} data-toggle={"modal"} data-target={`#likeModal`} className='ln'>0</div>
                    </div>
                    <div className='comment-group'>
                        <i className='material-icons'>mode_comment</i>
                        <div className='ln'>0</div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Comp;