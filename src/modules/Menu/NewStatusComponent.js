import React, { useState, useRef, useContext } from 'react';
import uuid from 'uuid';
import swal from 'sweetalert';
import ModalComponent from '../Common/ModalComponent';
import ImageIcon from '../Common/Icon/ImageIcon';
import { RouteContext } from '../../routes/routes';
import R from './../../locale/R';
import { GlobalStateContext } from '../Layout/HomeLayout';
import { type, upload, uploadFile } from './../../utils/StorageUtils';
import ReducerType from '../../reducers/type';
import { CON_TYPE, DB, fireStore } from './../../utils/DBUtils';
import UploadBackgroundComponent from '../Common/UploadBackgroundComponent';
import { isEmpty } from 'lodash';
import { showNotification } from '../../utils/NotificationUtils';
import StateManager from 'react-select';

const Comp = () => {

    const msgRef = useRef(null);
    const locationRef = useRef(null);
    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [localState, setLocalState] = useState({ wasHere: false, disabled: true });
    const [user, setUser] = useContext(RouteContext);

    const addStatusHandler = async () => {

        const { addNewMessage } = globalState;

        if (!msgRef || isEmpty(msgRef.current.value)) {
            showNotification('danger', R.new.err_message_null);
            return;
        }

        setLocalState({ ...localState, disabled: true });

        let data = {
            _id: uuid.v4(),
            [DB.STATUS.MESSAGE]: msgRef.current.value.substring(0, 3000),
            [DB.STATUS.USER_ID]: user.extData[0]._id,
            [DB.STATUS.WAS_HERE]: localState.wasHere,
            [DB.STATUS.IS_OWNER]: true,
        }

        // Upload background image
        if (!addNewMessage) {
            const coverBase64 = document.getElementById('base64BackgroundRaw').innerHTML;
            if (!coverBase64 || isEmpty(coverBase64)) {
                showNotification('danger', R.new.err_attach_image);
                return;
            }
            if (!isEmpty(coverBase64) && coverBase64.indexOf(';base64,') != -1) {
                let url = await upload(type.FILE + '/status/' + user.extData[0]._id + '/' + data._id, coverBase64, `main.jpg`);
                if (isEmpty(url)) {
                    showNotification('danger', R.new.err_cannot_upload_image);
                    return;
                } else {
                    data = { ...data, [DB.STATUS.BACKGROUND_IMG]: url, [DB.STATUS.LOCATION]: locationRef.current.value.substring(0, 45) }
                }
            }
        } else {
            data = { ...data, [DB.STATUS.IS_OWNER]: false, [DB.STATUS.IN_STATUS_ID]: globalState.status_id }
            dispatch({ type: ReducerType.HAS_NEW_MESSAGE, data });
        }

        await fireStore.update(DB.STATUS.name(), [DB.STATUS.ID, CON_TYPE.EQ, data._id], data, true);

        setLocalState({ ...localState, disabled: false });

        closeModalHandler(true);

        resetForm();
    }

    const resetForm = () => {
        msgRef.current.value = '';

        if(addNewMessage) return;

        locationRef.current.value = '';
        document.getElementById('id-avatar-edit').style.display = '';
        document.getElementById('upload-text-2').style.display = '';
        document.getElementById('upload-text-1').style.display = '';
        document.getElementById('id-tg').style.marginTop = 'unset';

        document.getElementById('id-iwashere').style.display = 'none';
        document.getElementById('id-add-location').style.display = 'none';
        document.getElementById('imageBackgroundPreview').style.display = 'none';
        document.getElementById('id-cancel-background').classList.add('hidden');
        document.getElementById('id-button-new-post').style.marginTop = '1rem';
        document.getElementById('id-txt-up-msg').style.height = '4.5rem';
    }

    const onChangeCheckboxHandler = e => {
        setLocalState({...localState, wasHere: e.target.checked });
    }

    const closeModalHandler = (force) => {

        if (force) {
            resetForm();
            document.getElementById('id-close-add-status-modal').click();
            return;
        }

        if (!msgRef.current.value && !locationRef.current.value && document.getElementById('imageBackgroundPreview').style.display === 'none') {
            resetForm();
            document.getElementById('id-close-add-status-modal').click();
            return;
        }
        swal({
            title: R.new.close_msg_1,
            text: R.new.close_msg_2,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((ok) => {
                if (ok) {
                    resetForm();
                    document.getElementById('id-close-add-status-modal').click();
                }
            });
    }

    const msgKeyUpHandler = e => {
        const { addNewMessage } = globalState;
        const value = e.target.value;
        if (addNewMessage) {
            if (isEmpty(value)) {
                setLocalState({ ...localState, disabled: true });
            } else {
                setLocalState({ ...localState, disabled: false });
            }
        } else {
            const coverBase64 = document.getElementById('base64BackgroundRaw').innerHTML;
            const imgPreviewDisplay = document.getElementById('imageBackgroundPreview').style.display;
            if (isEmpty(value) || isEmpty(coverBase64) || imgPreviewDisplay === 'none') {
                setLocalState({ ...localState, disabled: true });
            } else {
                setLocalState({ ...localState, disabled: false });
            }
        }
    }

    const uploadSuccessCallback = () => {
        const value = msgRef.current.value;
        const coverBase64 = document.getElementById('base64BackgroundRaw').innerHTML;
        const imgPreviewDisplay = document.getElementById('imageBackgroundPreview').style.display;
        if (isEmpty(value) || isEmpty(coverBase64) || imgPreviewDisplay === 'none') {
            setLocalState({ ...localState, disabled: true });
        } else {
            setLocalState({ ...localState, disabled: false });
        }
    }

    const { addNewMessage } = globalState;

    return (
        <ModalComponent style={!addNewMessage?{ height: '22rem' }:{height: '18.5rem'}} submitText={R.new.post} id='new_status_modal' hideHeader preventCloseWhenClickOut>
            <div onClick={closeModalHandler} className='close-modal'>
                <div className='cmi'>
                    <i className='material-icons'>close</i>
                </div>
            </div>
            <div className='message-group'>
                <div className='tit'>
                    <div className='txt'>
                        <i className='material-icons'>{addNewMessage ? 'mail' : 'note'}</i>
                        <span className='mif'>{addNewMessage ? R.new.title_new_msg : R.new.title}</span>
                    </div>
                </div>
                <div className='bod'>
                    <textarea onKeyUp={msgKeyUpHandler} ref={msgRef} placeholder={addNewMessage ? R.new.tapmsg : R.new.taplace} maxLength={3000}></textarea>
                </div>

                {/* New status */}
                {
                    !addNewMessage
                    &&
                    <div className='bot'>
                        <div className='txt' id='id-txt-up-msg' style={{ height: '4.5rem' }}>
                            <div className='t1' id='upload-text-1'>
                                <div className='svi'>
                                    <ImageIcon width={25} height={25} />
                                </div>
                                {R.new.up1}
                                <div className='required'>*</div>
                            </div>
                            <div className='t2' id='upload-text-2'>
                                {R.new.up2}
                                <br />
                                {R.new.up3}
                            </div>
                            <UploadBackgroundComponent uploadSuccessCallback={uploadSuccessCallback} />
                        </div>
                    </div>

                }
                {
                    !addNewMessage
                    &&
                    <div className='bot2'>
                        <div className='tg' id='id-tg'>
                            <div className='location' id='id-add-location' style={{ display: 'none' }}>
                                <div className='ing'>
                                    <i className='material-icons'>place</i>
                                    <input maxLength={45} ref={locationRef} type='text' placeholder='Enter the location...' />
                                </div>
                            </div>
                            <div className='ih' id='id-iwashere' style={{ display: 'none' }}>
                                <div className='ih1' onChange={onChangeCheckboxHandler}>
                                    <input type="checkbox" maxLength={100} className="form-check-input noselect" id="exampleCheck1" />
                                    <label className="form-check-label noselect" htmlFor="exampleCheck1">I was here</label>
                                </div>
                            </div>
                        </div>
                        <button disabled={localState.disabled} onClick={addStatusHandler} type='button' className='btg zbtn noselect' id='id-button-new-post' style={{ marginTop: '1rem' }}>
                            {R.new.post}
                        </button>
                    </div>
                }

                {/* New Message */}
                {
                    addNewMessage
                    &&
                    <div className='bot2'>
                        <div className='tg' id='id-tg'>
                            <div className='ih' style={{marginBottom:'-1rem'}}>
                                <div className='ih1' onChange={onChangeCheckboxHandler} style={{background:'unset', marginLeft:'-1rem'}}>
                                    <input type="checkbox" maxLength={100} className="form-check-input noselect" id="exampleCheck1" />
                                    <label className="form-check-label noselect" htmlFor="exampleCheck1">I was here</label>
                                </div>
                            </div>
                        </div>
                        <button disabled={localState.disabled} onClick={addStatusHandler} type='button' className='btg zbtn noselect' id='id-button-new-post' style={{ marginTop: '1rem' }}>
                            {R.new.post}
                        </button>
                    </div>
            
                }
                
            </div>
        </ModalComponent>
    )
}

export default Comp;