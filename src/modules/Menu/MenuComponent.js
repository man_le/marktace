import { isEmpty, isFunction } from 'lodash';
import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import 'moment/locale/vi';
import React, { useContext, useEffect, useRef, useState } from 'react';
import Avatar from 'react-avatar';
import { BounceLoader } from 'react-spinners';
import short from 'short-uuid';
import { Link, withRouter } from 'react-router-dom';
import type from '../../reducers/type';
import { RouteContext } from '../../routes/routes';
import BellIcon from '../Common/Icon/BellIcon';
import SettingIcon from '../Common/Icon/SettingIcon';
import { GlobalStateContext } from '../Layout/HomeLayout';
import R from './../../locale/R';
import NewStatusComponent from './NewStatusComponent';
import UserLikeModalComponent from './UserLikeModalComponent';
import Logo from './logo.png';
import './stylesheet.scss';

const MenuComponent = ({ history }) => {

    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [user, setUser] = useContext(RouteContext);
    const searchRef = useRef('');
    const [localState, setLocalState] = useState({ users: [], clothes: [], loading: false, showInBagDetail: false });

    const fetchData = async () => {

    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }

        return () => {
            return loadCompleted == true;
        }

    }, [user]);

    var trans = short();

    const showHideMessageHandler = () => {
        let { isHideMessage } = globalState;
        if (isHideMessage === true || isHideMessage === 'true') {
            isHideMessage = true;
        } else {
            isHideMessage = false;
        }
        history.push(`${window.location.pathname}?hideMsg=${!isHideMessage}`)
        dispatch({ type: type.SHOW_HIDE_MESSAGE, isHideMessage: !isHideMessage });
    }

    const addNewStatusHandler = () => {
        dispatch({ type: type.ADD_NEW_STATUS });
    }

    const signOut = (callback) => {
        firebase.auth().signOut().then(function () {
            if (isFunction(callback)) callback();
            else window.location.reload();
        }).catch(function (error) {
            console.error(error);
        });
    }

    const { isHideMessage } = globalState;

    return (
        <>
            <nav className="znav navbar navbar-expand-lg bg-danger nav-custom">
                <div className="container col-md-12">

                    <a className="navbar-brand zlogo" href="/">
                        <img src={Logo} alt="" />
                    </a>

                    <div className="form-inline">
                        <i className='material-icons'>search</i>
                        <input
                            maxLength={50}
                            ref={searchRef}
                            className={`searchbox ${!globalState.isOpenSearchDialog || (isEmpty(localState.users) && isEmpty(localState.clothes)) ? 'default' : ''}`}
                            onFocus={() => { }}
                            onKeyUp={() => { }}
                            type="search"
                            placeholder={R.common.search_all}
                            style={localState.loading ? { padding: '0.25rem 1.95rem 0.25rem 2.2rem' } : {}}
                        />

                        {
                            localState.loading
                            &&
                            <div className='load-in-textbox'>
                                <BounceLoader
                                    sizeUnit={"px"}
                                    size={20}
                                    loading={true}
                                    color='#777' />
                            </div>
                        }

                        {
                            globalState.isOpenSearchDialog && (!isEmpty(localState.users) || !isEmpty(localState.clothes))
                            &&
                            <div className='zsbox shadow'>
                                <div className='g nonhidden'>

                                    {
                                        localState.loading
                                            ?
                                            <div className='zloading-item'>
                                                <BounceLoader
                                                    sizeUnit={"px"}
                                                    size={50}
                                                    loading={true}
                                                    color='#777' />
                                            </div>
                                            :
                                            <div className='search-result-group nonhidden'>
                                                <div className='st'>
                                                    <div className='stg nonhidden'>
                                                        <div onClick={() => { dispatch({ type: type.CHANGE_SEARCH_ITEM, searchItem: 1 }) }} className={`stgi nonhidden ${globalState.searchItem === 1 ? 'active' : ''}`}>{R.common.people}</div>
                                                        <div onClick={() => { dispatch({ type: type.CHANGE_SEARCH_ITEM, searchItem: 2 }) }} className={`stgi nonhidden ${globalState.searchItem === 2 ? 'active' : ''}`}>{R.common.product}</div>
                                                    </div>
                                                </div>
                                                <div className='sc nonhidden'>
                                                </div>
                                            </div>

                                    }


                                </div>
                            </div>

                        }

                    </div>

                    <div className="collapse navbar-collapse">
                        <div className='menu-group'>
                            <div className='mnitem noselect'>
                                <Avatar className='zavatar of_cover' src={null} name={'Man Le'} size='2rem' round={true} />
                                <div className='mntext noselect'>Man Le</div>
                            </div>
                            <div className='spr'></div>
                            <div className='mnitem noselect'>
                                <div className='mntext noselect'>{R.menu.home}</div>
                            </div>
                            <div className='spr'></div>
                            <div className='mnitem noselect' data-toggle={"modal"} data-target={`#new_status_modal`} onClick={addNewStatusHandler}>
                                <div className='mntext noselect'>{R.menu.add}</div>
                            </div>
                            <div className='spr'></div>
                            <div className='mnitem noselect' onClick={showHideMessageHandler}>
                                {isHideMessage ? <i className='material-icons noselect'>visibility</i> : <i className='material-icons noselect'>visibility_off</i>}
                            </div>
                            <div className='spr'></div>
                            <div className='mnitem' style={{ marginLeft: '1rem' }}>
                                <BellIcon />
                            </div>
                            <div className='mnitem noselect' style={{ marginLeft: '-0.35rem' }}>
                                <a data-toggle="dropdown" aria-expanded="false">
                                    <SettingIcon />
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <Link className="dropdown-item" to="/setting/profile">
                                        {R.menu.profile}
                                    </Link>
                                    <Link className="dropdown-item" to="/setting/account">
                                        {R.menu.account}
                                    </Link>
                                    <Link className="dropdown-item" to="/setting/notification">
                                        {R.menu.notification}
                                    </Link>
                                    <div className="dropdown-divider"></div>
                                    <span onClick={signOut} href="#" className="dropdown-item zpointer">{R.menu.logout}</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </nav>

            <NewStatusComponent />
            <UserLikeModalComponent/>

        </>
    )
}

export default withRouter(MenuComponent);