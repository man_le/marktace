import React, {useState, useContext, useEffect} from 'react';
import { isEmpty } from 'lodash';
import R from './../../locale/R';
import { RouteContext } from '../../routes/routes';
import { GlobalStateContext } from '../Layout/HomeLayout';
import ModalComponent from '../Common/ModalComponent';
import { updateFollowing } from '../../reducers';
import { CON_TYPE, DB, fireStore, realTime} from './../../utils/DBUtils';
import UserItem from '../Common/UserItem';
const Comp = ({})=>{

    const [globalState, dispatch] = useContext(GlobalStateContext);
    const [user, setUser] = useContext(RouteContext);
    const [localState, setLocalState] = useState({
        followingUserIds:[], 
        hasInit:false,
    });

    const renderTotalLike = () => {
        const {likes} = globalState;
        return likes.length;
    }

    const followingHandler = async (isUnFollowing, followUserId) => {
        let newStateFollowingUserIds = localState.followingUserIds;
        if (isUnFollowing) {
            newStateFollowingUserIds = newStateFollowingUserIds.filter(e => e !== followUserId);
            await fireStore.remove(DB.FOLLOWING.name(),
                [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id],
                [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, followUserId]
            );
        } else {
            const data = {
                [DB.FOLLOWING.USER_ID_FROM]: user.extData[0]._id,
                [DB.FOLLOWING.USER_ID_TO]: followUserId,
            }
            if (newStateFollowingUserIds.indexOf(followUserId) === -1) {
                newStateFollowingUserIds.push(followUserId);
            }
            await fireStore.updateMultiConditions(
                DB.FOLLOWING.name(),
                data, true,
                [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id],
                [DB.FOLLOWING.USER_ID_TO, CON_TYPE.EQ, followUserId]
            );
        }

        updateFollowing(dispatch, { followingUserIds: newStateFollowingUserIds })

        setLocalState({ ...localState, followingUserIds: newStateFollowingUserIds });
    }

    const fetchData = async ()=>{
        let {followingUserIds} = localState;

        if (!isEmpty(user) && !isEmpty(user.extData) && !isEmpty(user.extData[0])) {

            let followingResult = await fireStore.find(DB.FOLLOWING.name(), [DB.FOLLOWING.USER_ID_FROM, CON_TYPE.EQ, user.extData[0]._id]);
            if (!isEmpty(followingResult)) {
                followingResult.map(e => {
                    if (followingUserIds.indexOf(e.userid_To) === -1) followingUserIds.push(e.userid_To)
                });
            }
            setLocalState({ ...localState, followingUserIds, hasInit: true });
        }
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }
        return () => loadCompleted == true;
    }, [
    ]);

    const {followingUserIds} = localState;

    return (
        <ModalComponent id={'likeModal'} title={`${R.modal.like} (${renderTotalLike()})`}>
                <div>
                    {/* {
                        isEmpty(globalState.likeUsers) && isEmpty(globalState.newLikes_users[globalState.currentStatus_id]) &&
                        <DataIsEmpty hideIcon={true} label={R.modal.no_like} color='#eee' />
                    }
                    */
                    
                        !isEmpty(globalState.likes)
                        &&
                        globalState.likes.map((e, i) =>
                            <UserItem modalId={'likeModal'} key={`like_${e}`} user_id={e}
                                hideActionMenu={e._id === user.extData[0]._id}
                                description={followingUserIds.indexOf(e) !== -1 && R.modal.following}
                                actionData={[
                                    {
                                        label: followingUserIds.indexOf(e) !== -1 ? R.modal.un_follow : R.modal.follow,
                                        onClickHandler: () => followingHandler(followingUserIds.indexOf(e) !== -1 ? true : false, e)
                                    }
                                ]} 
                                />
                        )
                    
                    /*
                    {
                        !isEmpty(globalState.newLikes_users) && globalState.currentStatus_id && !isEmpty(globalState.newLikes_users[globalState.currentStatus_id])
                        &&
                        globalState.newLikes_users[globalState.currentStatus_id].map((e, i) =>
                            <UserItem modalId={'ikeModal'} key={i} userData={e}
                                hideActionMenu={e._id === user.extData[0]._id}
                                description={globalState.followingUserIds.indexOf(e._id) !== -1 && R.modal.following}
                                actionData={[
                                    {
                                        label: globalState.followingUserIds.indexOf(e._id) !== -1 ? R.modal.un_follow : R.modal.follow,
                                        onClickHandler: () => followingHandler(globalState.followingUserIds.indexOf(e._id) !== -1 ? true : false, e._id)
                                    }
                                ]} />
                        )
                    } */}

                </div>
            </ModalComponent>
    )
}

export default Comp;