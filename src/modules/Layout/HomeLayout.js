import { isEmpty } from 'lodash';
import React, { useContext, useEffect, useReducer, useState } from 'react';
import { NotificationContainer } from 'react-notifications';
import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import globalReducer from '../../reducers';
import { buildKeyWords } from '../../utils/StringUtils';
import InActiveComponent from '../Common/InActiveComponent';
import ModalComponent from '../Common/ModalComponent';
import WelcomePageComponent from '../Common/WelcomePageComponent';
import MenuComponent from '../Menu/MenuComponent';
import R from './../../locale/R';
import { RouteContext } from './../../routes/routes';
import { CON_TYPE, DB, fireStore } from './../../utils/DBUtils';
import './stylesheet.scss';


export const GlobalStateContext = React.createContext([{}, () => { }]);

const HomeLayout = ({ children }) => {

  const [state, setState] = useState({ userSaveInDb:false});

  const params = new URLSearchParams(window.location.search);

  const initState = {
    hideWelcomePage: false,
    isHideMessage: params.get('hideMsg') === 'true',
    likes:[]
  };
  const [globalState, dispatch] = useReducer(globalReducer, initState);

  // Set user for the first times (after signup)
  const [user, setUser] = useContext(RouteContext);
  const fetchData = async () => {
    if(!state.userSaveInDb && !isEmpty(user) && user.uid){
      const data = {
        name: user.displayName,
        avatar: user.photoURL,
        userUid: user.uid,
        link: '',
        newLikes_userIds: {}, newLikes_users: {},
        notify_like_comment: true,
        notify_like_status: true,
        notify_share_status: true,
        notify_post_new: true,
        notify_comment: true,
        noti_following: true,
        email: user.email,
        social_id: !isEmpty(user.providerData) ? user.providerData[0].uid : null,
        phoneNumber: user.phoneNumber,
        keywords: buildKeyWords(user.displayName)
    }
      const isSuccess = await fireStore.update(DB.USER.name(), [DB.USER.USERUID, CON_TYPE.EQ, user.uid], data, true);
      if(isSuccess){
        const userInfo = await fireStore.find(DB.USER.name(), [DB.USER.USERUID, CON_TYPE.EQ, user.uid]);
        let loggedInTmp = user;
        loggedInTmp.extData = userInfo;
        setUser(loggedInTmp);
      }
      setState({...state, userSaveInDb:true});
    }
  }
  useEffect(() => {
    fetchData();
  }, [user])

  const { path } = children.props.match;
  return (
    <GlobalStateContext.Provider value={[globalState, dispatch]}>
      <ReactNotification />
      {
        !globalState.hideWelcomePage
          ?
          <WelcomePageComponent />
          :
          <div>
            {/* Menu */}
            {
              window.location.pathname.indexOf('_qa') !== -1 ||
                window.location.pathname.indexOf('_privacy') !== -1 ||
                window.location.pathname.indexOf('_about') !== -1 ||
                window.location.pathname.indexOf('_terms') !== -1
                ?
                null
                :
                <MenuComponent path={path} />
            }
            <div>
              {
                !isEmpty(user.extData) && user.extData[0].inactive
                  ?
                  <InActiveComponent user={user} />
                  :
                  children && children
              }
            </div>
            <NotificationContainer />
          </div>
      }
    </GlobalStateContext.Provider>
  );
}

export default HomeLayout;
