import React from 'react';
import Resizer from 'react-image-file-resizer';

const readURL = (input) => {
  if (input.target.files && input.target.files[0]) {

    // Resize image
    let file = input.target.files[0];
    Resizer.imageFileResizer(
      file,
      932,
      688,
      'JPEG',
      100,
      0,
      uri => {
        const reader = new FileReader();
        reader.onload = async (e) => {
          document.getElementById('base64CoverRaw').innerHTML = uri;
          document.getElementById('imageCoverPreview').style.backgroundImage = 'url(' + uri + ')';
        };
        reader.readAsArrayBuffer(file);
      },
      'base64'
    );
  }
}

const UploadCoverComponent = ({ defaultUrl }) => {
  return (
    <div className="avatar-upload cover">
      <div className="avatar-edit">
        <input onChange={input => readURL(input)} type='file' id="imageCoverUpload" accept=".png, .jpg, .jpeg" />
        <label htmlFor="imageCoverUpload"></label>
      </div>
      <div className="avatar-preview cover">
        <div className='cover' id="imageCoverPreview" style={{ backgroundImage: `url('${defaultUrl}')` }}>
        </div>

        <div className='hidden' id='base64CoverRaw'>
          {defaultUrl}
        </div>

      </div>
    </div>
  )
}

export default UploadCoverComponent;