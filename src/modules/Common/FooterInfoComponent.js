import React from 'react';
import { Link } from 'react-router-dom';
import R from '../../locale/R';

const FooterInfoComponent = () => {
    return (
        <div className='row fixed-footer'>
            <div className='col-md-3 l'>
            </div>
            <div className='col-md-6'></div>
            <div className='col-md-2 r'>
                <div className='zcont noselect'>
                    <Link to='/_privacy'>
                        {R.access.policy}
                    </Link>
                </div>
                <div className='zcont noselect'>
                    <Link to='/_terms'>
                        {R.access.term}
                    </Link>
                </div>

                <div className='zcont noselect'>
                    <Link to='/_qa'>
                        {R.access.qa}
                    </Link>
                </div>

                <div className='zline noselect'>
                </div>

                <div className='zcont noselect'>
                    <Link to='/_about'>
                        ©Zapii.me {new Date().getFullYear()}
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default FooterInfoComponent