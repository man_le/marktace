import React from 'react';
import Resizer from 'react-image-file-resizer';

const readURL = (input, uploadSuccessCallback) => {
  if (input.target.files && input.target.files[0]) {

    // Resize image
    let file = input.target.files[0];
    Resizer.imageFileResizer(
      file,
      932,
      688,
      'JPEG',
      100,
      0,
      uri => {
        const reader = new FileReader();
        reader.onload = (e) => {
          document.getElementById('base64BackgroundRaw').innerHTML = uri;
          document.getElementById('imageBackgroundPreview').style.backgroundImage = 'url(' + uri + ')';
          document.getElementById('imageBackgroundPreview').style.display = '';

          document.getElementById('id-avatar-edit').style.display = 'none';
          document.getElementById('upload-text-2').style.display = 'none';
          document.getElementById('upload-text-1').style.display = 'none';
          
          document.getElementById('id-iwashere').style.display = '';
          document.getElementById('id-add-location').style.display = '';
          document.getElementById('id-cancel-background').classList.remove('hidden');

          document.getElementById('id-button-new-post').style.marginTop = 'unset';
          document.getElementById('id-txt-up-msg').style.height = 'unset';
          document.getElementById('id-tg').style.marginTop = '-2.5rem';

          uploadSuccessCallback();
        };
        reader.readAsArrayBuffer(file);

      },
      'base64'
    );

  }
}

const UploadBackgroundComponent = ({ uploadSuccessCallback }) => {

  const cancelBackgroundImageHandler = () => {
    document.getElementById('id-avatar-edit').style.display = '';
    document.getElementById('upload-text-2').style.display = '';
    document.getElementById('upload-text-1').style.display = '';
    document.getElementById('id-tg').style.marginTop = 'unset';

    document.getElementById('id-iwashere').style.display = 'none';
    document.getElementById('id-add-location').style.display = 'none';
    document.getElementById('imageBackgroundPreview').style.display = 'none';
    document.getElementById('id-cancel-background').classList.add('hidden');
    document.getElementById('id-button-new-post').style.marginTop = '1rem';
    document.getElementById('id-txt-up-msg').style.height = '4.5rem';

    uploadSuccessCallback();
  }

  return (
    <div className="background-upload">
      <div className="avatar-edit" id='id-avatar-edit'>
        <input onChange={input => readURL(input, uploadSuccessCallback)} onClick={e=>{e.target.value = null}} type='file' id="base64BackgroundUpload" accept=".png, .jpg, .jpeg" />
        <label htmlFor="base64BackgroundUpload"></label>
      </div>
      <div className="background-preview">
        <i onClick={cancelBackgroundImageHandler} className='material-icons hidden' id='id-cancel-background'>cancel</i>
        <div className='prv' id="imageBackgroundPreview" style={{display:'none' }}></div>
        <div className='hidden' id='base64BackgroundRaw'></div>
      </div>
    </div>
  )
}

export default UploadBackgroundComponent;