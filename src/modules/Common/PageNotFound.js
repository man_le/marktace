import React from 'react';
import R from '../../locale/R';

const PageNotFound = () => {
    return (
        <div className='page-not-found'>
            <i className='material-icons'>error</i>
            <div className='t'>{R.common.page_not_found}</div>
        </div>
    )
}

export default PageNotFound;