import React from 'react';
import { BounceLoader } from 'react-spinners';

const LoadingAllPageComponent = () => {
    return (
        <div style={{ display: 'flex', justifyContent: 'center', background:'rgba(255,255,255,0.8)', position:'absolute', top:0, width:'100%', height:'100%' }}>
            <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
                <BounceLoader
                    sizeUnit={"px"}
                    size={50}
                    loading={true}
                    color='#777'
                />
            </div>
        </div>
    )
}

export default LoadingAllPageComponent;