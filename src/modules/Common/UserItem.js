import React, { useEffect, useState } from 'react';
import './stylesheet.scss';
import short from 'short-uuid';
import { isEmpty } from 'lodash';
import Avatar from 'react-avatar';
import { Link } from 'react-router-dom';
import MoreActionComponent from './MoreActionComponent';
import UserItemLoader from './UserItemLoader';
import { fireStore, CON_TYPE, DB } from '../../utils/DBUtils';

const UserItem = ({ type, modalId, user_id, description, content_id, actionData, hideActionMenu, extClass }) => {

    const translator = short();
    const [localState, setLocalState] = useState({ userData: {} })

    const fetchData = async () => {
        let userData = await fireStore.find(DB.USER.name(), [DB.USER.ID, CON_TYPE.EQ, user_id]);
        if(!isEmpty(userData)) userData = userData[0];
        setLocalState({...localState, userData});
    }

    useEffect(() => {
        let loadCompleted = false;
        if (!loadCompleted) {
            fetchData();
            loadCompleted = true;
        }

        return () => {
            return loadCompleted == true;
        }

    }, []);

    const {userData} = localState;
    if(isEmpty(userData)) return null;

    return (

        <div className={`user-item-group ${extClass && extClass}`}>
            <div className='i-av-g'>
                <Link onClick={() => { modalId && document.getElementById(`${modalId}`).click() }} to={`/${translator.fromUUID(userData._id)}`}>
                    <Avatar className='i-av of_cover' src={userData.avatar} name={userData.name} size='3rem' round={true} />
                </Link>
            </div>
            <div className='i-txt-g'>
                <Link onClick={() => { modalId && document.getElementById(`${modalId}`).click() }} to={`/${!content_id ? translator.fromUUID(userData._id) : (type !== 'following' || !type ? 'status/' : '') + translator.fromUUID(content_id)}`}>
                    <div className='i-txt-1 noselect'><strong>{userData.name}</strong> {userData.label}</div>
                    {
                        description && <div className='i-txt-2 noselect'>{description}</div>
                    }
                </Link>
            </div>
            {
                !hideActionMenu
                &&
                <MoreActionComponent data={actionData} />
            }

        </div>
    )
}

export default UserItem;
