import React from 'react';
const ClockIcon = () => {
    return (
        <svg x="0px" y="0px" width="32px" height="32px" fill="#f2f2f2" viewBox="0 0 64 64" enable-background="new 0 0 64 64"><g><path fill="none" stroke="#a6a6a6" stroke-width="3" stroke-miterlimit="10" d="M53.92,10.081 c12.107,12.105,12.107,31.732,0,43.838c-12.106,12.108-31.734,12.108-43.84,0c-12.107-12.105-12.107-31.732,0-43.838 C22.186-2.027,41.813-2.027,53.92,10.081z"></path><polyline fill="none" stroke="#a6a6a6" stroke-width="3" stroke-miterlimit="10" points="32,12 32,32 41,41"></polyline></g></svg>
    )
}

export default ClockIcon;
