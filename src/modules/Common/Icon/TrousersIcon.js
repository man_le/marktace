import React from 'react';
const TrousersIcon = ({width, height, style, color, className}) => {
	return(
		<svg height={height||'22'} viewBox="0 0 128 128" width={width||'22'} style={style} className={className}><path d="m103.81 121.5h-24.98l-13.08-78.06-1.75-10.44-1.75 10.44-13.08 78.06h-24.98l9.19-95.58.34-3.48 1.53-15.94h57.5l1.53 15.94.34 3.48z" fill={color||'#262626'}/><g fill='#fff'><path d="m93.45 13.82h-58.9l.7-7.32h57.5z"/><path d="m94.62 25.92a.467.467 0 0 1 -.17.02h-2.79a11.73 11.73 0 0 1 -11.69-10.48l-.15-1.45a1.75 1.75 0 1 1 3.48-.38l.15 1.45a8.246 8.246 0 0 0 8.21 7.36h2.62z"/><path d="m48.18 14.01-.15 1.45a11.73 11.73 0 0 1 -11.69 10.48h-2.79a.467.467 0 0 1 -.17-.02l.34-3.48h2.62a8.246 8.246 0 0 0 8.21-7.36l.15-1.45a1.75 1.75 0 1 1 3.48.38z"/><path d="m65.75 13.82v29.62l-1.75-10.44-1.75 10.44v-29.62z"/></g></svg>
	)
}

export default TrousersIcon;
