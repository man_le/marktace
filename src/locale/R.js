import cookies from 'js-cookie';

import en from './R_en';
import vi from './R_vi';

let R ;
if(cookies.get('z_lang')==='vi') R = vi;
else R = vi;
// else R = en;

export default R;

