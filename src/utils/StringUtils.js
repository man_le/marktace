import { getLang } from "./CookieUtils";
import {isEmpty} from 'lodash';
import { isEmptyStatement } from "@babel/types"

export const formatMoment = (moment, milisecond) =>{
    switch(getLang()){
        case 'vi':
            return moment(milisecond).format("DD/MM/YYYY [lúc] HH:mm")
        default:
            return moment(milisecond).format("DD/MM/YYYY [at] HH:mm") 
    }
}

export const formatMomentNoTime = (moment, milisecond) =>{
    return moment(milisecond).format("DD/MM/YYYY")
}

export const buildKeyWords = text =>{
    text = removeAccents(text);
    const split = text.split(' ');
    const keywords = [];
    split.forEach(e=>{
        const s = e.split('');
        let join = '';
        s.forEach(ei=>{
            join += ei;
            keywords.push(join);
        })
    });
    return keywords;
}

export const removeAccents = str => {
    var from = "àáãảạăằắẳẵặâầấẩẫậèéẻẽẹêềếểễệđùúủũụưừứửữựòóỏõọôồốổỗộơờớởỡợìíỉĩịäëïîöüûñç",
        to   = "aaaaaaaaaaaaaaaaaeeeeeeeeeeeduuuuuuuuuuuoooooooooooooooooiiiiiaeiiouunc";
    for (var i=0, l=from.length ; i < l ; i++) {
      str = str.replace(RegExp(from[i], "gi"), to[i]);
    }
  
    str = str.toLowerCase().trim()
  
    return str;
  }


export const answerOpts = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 
        'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 
        'U', 'V', 'W', 'X', 'Y', 'Z' 
    ]

export const compareString = (s1, s2) => {
    if(isEmpty(s1) || isEmpty(s2)) return 0;
    const spl1 = s1.split('');
    const spl2 = s2.split('');
    if(spl1.length == spl2.length){
        const n = spl1.length;
        let countSame = 0;
        for(let i=0;i<n;i++){
            if(spl1[i] === spl2[i]) countSame++;
        }
        return Math.floor((100*countSame)/n);
    }else return 0;
}

export const getQuestionTypeSame = (s1, s2, questionTypeString) => {
    if(isEmpty(s1) || isEmpty(s2) || isEmpty(questionTypeString)) return [];
    
    const spl1 = s1.split('');
    const spl2 = s2.split('');
    const quesSpl = questionTypeString.split(',');
    const result = [];
    if(spl1.length == spl2.length){
        const n = spl1.length;
        for(let i=0;i<n;i++){
            if(spl1[i] === spl2[i] && result.indexOf(quesSpl[i]) === -1)  result.push(quesSpl[i])
        }
    }
    return result;
}


export const getIndexOfAnswer = (answer) =>{
    return answerOpts.findIndex(e=>e===answer);
}