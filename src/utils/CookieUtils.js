import cookies from 'js-cookie';
import {isEmpty} from 'lodash';

export const getLang = ()=>{
    let value = cookies.get('z_lang');
    if(isEmpty(value)){
        return 'vi';
    } 
    return value;
}