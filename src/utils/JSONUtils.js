export const buildProductFilterTypes = (type) =>{
    switch(type){
        case 'women':
            return [
                'all', 'shirt', 'tshirt', 'jacket', 'hat', 'shoes', 'trousers', 'short', 'bag', 'dress'
            ]
        default: //man
            return [
                'all', 'shirt', 'tshirt', 'jacket', 'hat', 'shoes', 'trousers', 'short', 'bag'
            ]
    }
}

// Check product have size or not
export const productHaveItems = item =>{
    if(item.XS && item.XS-1 > 0) return true;
    else if(item.S && item.S-1 > 0) return true;
    else if(item.M && item.M-1 > 0) return true;
    else if(item.L && item.L-1 > 0) return true;
    else if(item.XL && item.XL-1 > 0) return true;
    for(let i=32;i<45;i++){
        if(item[i] && item[i]-1 > 0) return true;
    }
    return false;
}