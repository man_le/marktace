import {isEmpty} from 'lodash';

const URL_REGEX = [
    '/setting/profile/', '/setting/account/', '/setting/notification/', '/new/', '/status/'
]

export const activeSettingIcons = (url) => {
    for(var i=0;i<URL_REGEX.length;i++){
        if(url.indexOf(URL_REGEX[i])!=-1) return true;
    }
    return false;
}

export const getPrefixURL = ()=>{
    return `${window.location.protocol}//${window.location.hostname}${!isEmpty(window.location.port)?`:${window.location.port}`:''}`
}