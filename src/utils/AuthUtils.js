import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import _ from 'lodash';

const firebaseConfig = {
    apiKey: "AIzaSyD-jB_SRVLro7m8h2OVautk-sqWk5snCdE",
    authDomain: "marktace-com.firebaseapp.com",
    databaseURL: "https://marktace-com.firebaseio.com",
    projectId: "marktace-com",
    storageBucket: "marktace-com.appspot.com",
    messagingSenderId: "822480784015",
    appId: "1:822480784015:web:0a45da8de00ec390e3fefa",
    measurementId: "G-53N7S0QEFK"
};

const init = () =>{
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }
}

init();

const loggedHandler = (loggedInCb, notLoggedInCallback) => {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            loggedInCb(user);
        } else {
            notLoggedInCallback();
        }
    });
}

const ggProvider = new firebase.auth.GoogleAuthProvider();
const googleLogin = async (callback) => {
    try {
        let result = await firebase.auth().signInWithRedirect(ggProvider);
        let user = result.user;

        if (!_.isEmpty(user)) {
            if (_.isFunction(callback)) callback(null, user);
        }
    } catch (err) {
        var code = err.code;
        console.error(err);
        callback(code);
    }
}

var fbProvider = new firebase.auth.FacebookAuthProvider();
const facebookLogin = async (callback) => {
    try {
        let result = await firebase.auth().signInWithRedirect(fbProvider);

        let user = result.user;
        if (!_.isEmpty(user)) {
            if (_.isFunction(callback)) callback(null, user);
        }
    } catch (err) {
        var code = err.code;
        console.error(err);
        callback(code);
    }
}

const linkAccount = async (type, callback) => {
    let provider = ggProvider;
    switch (type) {
        case 'facebook':
            provider = fbProvider;
            break;
        case 'google':
            provider = ggProvider;
            break;
    }

    firebase.auth().currentUser.linkWithPopup(provider).then(function (result) {
        callback();
    }).catch(function (err) {
        console.error(err);
        callback(err.code);
    });
}

const unLinkAccount = async (type, callback) => {
    let provider = null;
    switch (type) {
        case 'facebook':
            provider = 'facebook.com';
            break;
        case 'google':
            provider = 'google.com';
            break;
    }

    firebase.auth().currentUser.unlink(provider).then(function (result) {
        callback();
    }).catch(function (err) {
        callback(err.code)
    });
}

const signOut = (callback) => {
    firebase.auth().signOut().then(function () {
        if (_.isFunction(callback)) callback();
        else window.location.reload();
    }).catch(function (error) {
        console.error(error);
    });
}

const createAcc = async (email, password, callback) => {
    try {
        let userInfo = await firebase.auth().createUserWithEmailAndPassword(email, password);
        callback(null, userInfo.user);
    } catch (err) {
        var code = err.code;
        console.error(err);
        callback(code);
    }
}

const updatePassword = async (email, oldPass, newPass, callback) => {

    try {
        let useInfo = await firebase.auth().signInWithEmailAndPassword(email, oldPass);
        useInfo.user.updatePassword(newPass);
        callback();
    } catch (err) {
        var code = err.code;
        console.error(err);
        callback(code);
    }

}

const updateEmail = async (email, oldPass, newEmail, callback) => {
    try {
        let useInfo = await firebase.auth().signInWithEmailAndPassword(email, oldPass);
        const result = useInfo.user.updateEmail(newEmail);
        callback();
    } catch (err) {
        var code = err.code;
        console.error(err);
        callback(code);
    }
}

const login = async (email, password, callback) => {
    try {
        let useInfo = await firebase.auth().signInWithEmailAndPassword(email, password);
        callback();
    } catch (err) {
        var code = err.code;
        console.error(err);
        callback(code);
    }
}

const forgotPass = (emailAddress, callback) => {
    let auth = firebase.auth();
    auth.sendPasswordResetEmail(emailAddress).then(function () {
        callback();
    }).catch(function (err) {
        console.error(err);
        callback(err.code);
    });
}

const isLoggedIn = (loggedIn) =>{
    return loggedIn.isLogged;
}

export {
    init,
    loggedHandler,
    login,
    googleLogin,
    facebookLogin,
    signOut,
    createAcc,
    updatePassword,
    updateEmail,
    forgotPass,
    linkAccount,
    unLinkAccount,

    isLoggedIn
};
