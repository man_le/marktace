import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { isEmpty } from 'lodash';
import registerServiceWorker, {unregister} from './registerServiceWorker';
import RoutesComponent from './routes/routes';
import { loggedHandler } from './utils/AuthUtils';
import { fireStore, DB, CON_TYPE } from './utils/DBUtils';
// import { unregister } from './serviceWorker';

// window.addEventListener('error', function (e) {
//   console.log(e);
// });

const routerHandler = async (loggedIn) => {

  let extData = null;

  if (!isEmpty(loggedIn)) {
    
    // Build extData from loggedIn
    extData = [{
      avatar: loggedIn.photoURL,
      userUid: loggedIn.uid,
      name: loggedIn.displayName,
    }];

    // Get extData from db (if exist)
    const uid = loggedIn.uid;
    const tempData = await fireStore.find(DB.USER.name(), [DB.USER.USERUID, CON_TYPE.EQ, uid]);
    if (!isEmpty(tempData)) {
      extData = tempData;
    }

  }

  ReactDOM.render(
    <RoutesComponent loggedIn={{ ...loggedIn, extData, isLogged:!isEmpty(loggedIn) }} />
    ,
    document.getElementById('root')
  )
}

loggedHandler(

  // Logged in
  (user) => routerHandler(user),

  // Not logged in
  () => routerHandler(false),

)


// registerServiceWorker();
unregister();

